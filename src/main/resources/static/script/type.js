var currentTypeTitle = '';

$(document).ready(function () {
    $('#createType').click(function () {
        $.ajax({
            url: "http://localhost:8080/type",
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                title: $('#type-title').val('')
            }),
            success: function (type) {
                console.log(type)
                alert("type with name" + type.title + "created")
                $('#type-title').val('')
                $('#getTitle').click();
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })
    })
    $('#getType').click(function () {
        $.ajax({
            url: 'http://localhost:8080/type',
            type: 'get',
            success: function (type) {
                console.log(type)
                $('.type').html(JSON.stringify(type))
                $('#type-list').html(type.map(function(t){
                    return renderTypeObject(t)
                }))
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })
    })
})

function renderTypeObject(type){
    var htmlString = "<button " + 'onclick="onTypeClick(' + "\'" + type.title + "\'" + ')"><h3>' + type.title + "</h3></button>";
    console.log(htmlString);
    return htmlString;
}

function onTypeClick(title) {
    console.log(title);
    currentTypeTitle = title
}

function getTypeTitle() {
    return currentTypeTitle;
}