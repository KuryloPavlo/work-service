$(document).ready(function () {
    $('#createSubType').click(function () {
        $.ajax({
            url: "http://localhost:8080/subType",
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                title: $('#subType-title').val(),
                typeTitle: getTypeTitle()
            }),
            success: function (subType) {
                console.log(subType)
                alert("subType with name" + subType.title + "created")
                $('#subType-title').val()
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })
    })
    $('#getSubType').click(function () {
        $.ajax({
            url: 'http://localhost:8080/subType',
            type: 'get',
            success: function (subType) {
                console.log(subType)
                $('.subType').html(JSON.stringify(subType))
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })
    })
})