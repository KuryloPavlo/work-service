$(document).ready(function () {
    $('#createLocation').click(function () {
        $.ajax({
            url: "http://localhost:8080/location",
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                region: $('#location-region').val(),
                city: $('#location-city').val()
            }),
            success: function (location) {
                console.log(location)
                alert("location with region" + location.region + " and city" + location.city + "created")
                $('#location-region').val('')
                $('#location-city').val('')
                $('#getLocation').click();
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })

    })
    $('#getLocation').click(function () {
        $.ajax({
            url: 'http://localhost:8080/location',
            type: 'get',
            success: function (location) {
                console.log(location)
                $('.location').html(JSON.stringify(location))
            },
            error: function (e) {
                console.log(e)
                alert(e.responseJSON.message)
            }
        })

    })

})