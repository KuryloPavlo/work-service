package pavlo.kurylo.workservice.exseption;

public class InputDataException extends Exception {
    public InputDataException() {
    }

    public InputDataException(String message) {
        super(message);
    }
}
