package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.request.ServicesRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.ServicesResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.ServicesService;

import java.util.List;

@RestController
@RequestMapping("/services")
public class ServicesController {

    @Autowired
    private ServicesService servicesService;

    @SneakyThrows
    @PostMapping
    public ServicesResponse save(@RequestBody ServicesRequest servicesRequest){
        return servicesService.save(servicesRequest);
    }

    @PostMapping("/page")
    public DataResponse<ServicesResponse> getPage(@RequestBody PaginationRequest paginationRequest){
        return servicesService.findAll(paginationRequest);}

    @SneakyThrows
    @PutMapping
    public ServicesResponse update(@RequestParam Long id,@RequestBody ServicesRequest servicesRequest){
        return servicesService.update(servicesRequest,id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id){servicesService.delete(id);}
}
