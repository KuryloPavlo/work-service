package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.CommentRequest;
import pavlo.kurylo.workservice.dto.response.CommentResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.CommentService;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @SneakyThrows
    @PostMapping
    public CommentResponse save(@RequestBody CommentRequest commentRequest){
        return commentService.save(commentRequest);
    }

    @GetMapping
    public List<CommentResponse> findAll(){return commentService.findAll();}

    @SneakyThrows
    @PutMapping
    public CommentResponse update(@RequestParam Long id,@RequestBody CommentRequest commentRequest){
        return commentService.update(commentRequest,id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id){commentService.delete(id);}
}
