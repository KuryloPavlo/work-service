package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.request.TypeRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.TypeResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.TypeService;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
    private TypeService typeService;

    @CrossOrigin
    @PostMapping
    public TypeResponse save(@RequestBody TypeRequest typeRequest){
        return typeService.save(typeRequest);
    }

    @CrossOrigin
    @GetMapping
    public List<TypeResponse> findAll(){return typeService.findAll();}

    /*
    @PostMapping("/page")
    public DataResponse<TypeResponse> findAll(@RequestBody PaginationRequest paginationRequest){
        return typeService.findAll(paginationRequest);}*/

    @SneakyThrows
    @PutMapping
    public TypeResponse update(@RequestParam Long id,@RequestBody TypeRequest typeRequest){
        return typeService.update(typeRequest,id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id){typeService.delete(id);}
}
