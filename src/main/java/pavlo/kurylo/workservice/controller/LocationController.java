package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.LocationRequest;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.LocationResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.LocationService;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @CrossOrigin
    @PostMapping
    public LocationResponse save(@RequestBody LocationRequest locationRequest){
        return locationService.save(locationRequest);
    }

    /*@GetMapping
    public DataResponse<LocationResponse> findAll(@RequestBody PaginationRequest paginationRequest){
        return locationService.findAll(paginationRequest);
    }*/
    @CrossOrigin
    @GetMapping
    public List<LocationResponse> findAll(){return locationService.findAll();}


    @SneakyThrows
    @PutMapping
    public LocationResponse update(@RequestParam Long id, @RequestBody LocationRequest locationRequest){
        return locationService.update(locationRequest, id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id){locationService.delete(id);}

}
