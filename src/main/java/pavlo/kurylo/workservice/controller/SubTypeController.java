package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.SubTypeRequest;
import pavlo.kurylo.workservice.dto.response.SubTypeResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.SubTypeService;

import java.util.List;

@RestController
@RequestMapping("/subType")
public class SubTypeController {

    @Autowired
    private SubTypeService subTypeService;

    @CrossOrigin
    @PostMapping
    public SubTypeResponse save(@RequestBody SubTypeRequest subTypeRequest){
        return subTypeService.save(subTypeRequest);
    }

    @CrossOrigin
    @GetMapping
    public List<SubTypeResponse> findAll(){return subTypeService.findAll();}

    @SneakyThrows
    @PutMapping
    public SubTypeResponse update(@RequestParam Long id,@RequestBody SubTypeRequest subTypeRequest){
        return subTypeService.update(subTypeRequest, id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id){subTypeService.delete(id);}
}
