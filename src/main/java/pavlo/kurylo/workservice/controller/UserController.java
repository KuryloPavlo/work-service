package pavlo.kurylo.workservice.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pavlo.kurylo.workservice.dto.request.UserRequest;
import pavlo.kurylo.workservice.dto.response.UserResponse;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public UserResponse save(@RequestBody UserRequest userRequest){
        return userService.save(userRequest);
    }

    @GetMapping
    public List<UserResponse> findAll(){return userService.findAll();}

    @SneakyThrows
    @PutMapping
    public UserResponse update(@RequestParam Long id, @RequestBody UserRequest userRequest){
        return userService.update(userRequest,id);
    }

    @SneakyThrows
    @DeleteMapping
    public void delete(@RequestParam Long id)throws InputDataException{userService.delete(id);}
}
