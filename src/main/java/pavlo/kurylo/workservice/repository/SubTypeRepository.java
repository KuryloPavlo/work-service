package pavlo.kurylo.workservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pavlo.kurylo.workservice.entity.SubType;


@Repository
public interface SubTypeRepository extends JpaRepository<SubType, Long> {
}
