package pavlo.kurylo.workservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pavlo.kurylo.workservice.entity.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long>{

    Location findByRegion(String region);
    Location findByCity(String city);
}
