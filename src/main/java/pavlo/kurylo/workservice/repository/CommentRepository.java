package pavlo.kurylo.workservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pavlo.kurylo.workservice.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{
}
