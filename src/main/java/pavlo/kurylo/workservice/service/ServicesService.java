package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.request.ServicesRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.ServicesResponse;
import pavlo.kurylo.workservice.entity.Services;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.ServiceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServicesService {

    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private TypeService typeService;

    public Services findOne(Long id)throws InputDataException{
        return serviceRepository.findById(id).orElseThrow(() -> new InputDataException("Services with id" + id + "not exists"));
    }

    public ServicesResponse save(ServicesRequest servicesRequest)throws InputDataException{
        return new ServicesResponse(serviceRepository.save(servicesRequestToServices(servicesRequest,null)));
    }

    public ServicesResponse update(ServicesRequest servicesRequest,Long id) throws InputDataException{
        return new ServicesResponse(serviceRepository.save(servicesRequestToServices(servicesRequest,findOne(id))));
    }

    public DataResponse<ServicesResponse> findAll(PaginationRequest paginationRequest){
        Page<Services> page = serviceRepository.findAll(paginationRequest.toPageRequest());

        return new DataResponse<>(
                page.get().map(ServicesResponse::new).collect(Collectors.toList()),
                page.getTotalPages(),
                page.getTotalElements()
        );
    }


    public void delete(Long id) throws InputDataException{
        serviceRepository.delete(findOne(id));
    }

    private Services servicesRequestToServices(ServicesRequest servicesRequest, Services services)throws InputDataException{
        if(services == null){
            services = new Services();
        }

        services.setTitle(servicesRequest.getTitle());
        services.setDescription(servicesRequest.getDescription());
        services.setSalary(servicesRequest.getSalary());
        services.setJobSearch(servicesRequest.isJobSearch());
        services.setUser(userService.findOne(servicesRequest.getUserId()));
        services.setLocation(locationService.findOne(servicesRequest.getLocationId()));
        services.setType(typeService.findByTitle(servicesRequest.getTypeTitle()));
        return services;
    }


}
