package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.request.TypeRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.TypeResponse;
import pavlo.kurylo.workservice.entity.Type;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.TypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeService {

    @Autowired
    private TypeRepository typeRepository;

    public TypeResponse save(TypeRequest typeRequest){
        return  new TypeResponse(typeRepository.save(typeRequestToType(typeRequest,null)));
    }

    public TypeResponse update(TypeRequest typeRequest, Long id)throws InputDataException {
        return new TypeResponse(typeRepository.save(typeRequestToType(typeRequest,findOne(id))));
    }

    public void delete(Long id) throws InputDataException {
        typeRepository.delete(findOne(id));
    }

    public List<TypeResponse> findAll(){
        return typeRepository.findAll().stream()
                .map(TypeResponse::new)
                .collect(Collectors.toList());
    }

    /*public DataResponse<TypeResponse> findAll(PaginationRequest paginationRequest){
        Page<Type> page = typeRepository.findAll(paginationRequest.toPageRequest());

        return new DataResponse<>(
                page.get().map(TypeResponse::new).collect(Collectors.toList()),
                page.getTotalPages(),
                page.getTotalElements()
        );
    }*/

    private Type findOne(Long id)throws InputDataException {
        return typeRepository.findById(id).orElseThrow(()->new InputDataException("Type with id" + id + "not exists"));
    }

    private Type typeRequestToType(TypeRequest typeRequest, Type type){
        if (type == null){
            type = new Type();
        }
        type.setTitle(typeRequest.getTitle());
        return type;
    }

    public Type findByTitle(String title){
        return typeRepository.findByTitle(title);
    }
}