package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.UserRequest;
import pavlo.kurylo.workservice.dto.response.UserResponse;
import pavlo.kurylo.workservice.entity.User;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserResponse save(UserRequest userRequest){
        return new UserResponse(userRepository.save(userRequestToUser(userRequest, null)));
    }

    public UserResponse update(UserRequest userRequest, Long id)throws InputDataException {
        return new UserResponse(userRepository.save(userRequestToUser(userRequest, findOne(id))));
    }

    public void delete(Long id) throws InputDataException{
        userRepository.delete(findOne(id));
    }

    public List<UserResponse> findAll(){
        return userRepository.findAll().stream()
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    public User findOne(Long id)throws InputDataException{
        return userRepository.findById(id).orElseThrow(() -> new InputDataException("User with id" + id + "not exists"));
    }

    private User userRequestToUser(UserRequest userRequest, User user){
        if (user == null){
            user = new User();
        }
        user.setName(userRequest.getName());
        user.setPassword(userRequest.getPassword());
        user.setEmail(userRequest.getEmail());
        user.setPhoneNumber(userRequest.getPhoneNumber());
        return user;
    }

}
