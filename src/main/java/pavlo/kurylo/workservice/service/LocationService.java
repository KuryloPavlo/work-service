package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.LocationRequest;
import pavlo.kurylo.workservice.dto.request.PaginationRequest;
import pavlo.kurylo.workservice.dto.response.DataResponse;
import pavlo.kurylo.workservice.dto.response.LocationResponse;
import pavlo.kurylo.workservice.entity.Location;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.LocationRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public LocationResponse save(LocationRequest locationRequest){
        return new LocationResponse(locationRepository.save(locationRequestToLocation(locationRequest, null)));
    }

    public LocationResponse update(LocationRequest locationRequest,Long id) throws InputDataException {
        return new LocationResponse(locationRepository.save(locationRequestToLocation(locationRequest,findOne(id))));
    }

    public void delete(Long id) throws InputDataException {
        locationRepository.delete(findOne(id));
    }

    private Location locationRequestToLocation(LocationRequest request, Location location){
        if(location == null){
            location = new Location();
        }
        location.setRegion(request.getRegion());
        location.setCity(request.getCity());
        return location;
    }

    public Location findOne(Long id) throws InputDataException {
        return locationRepository.findById(id).orElseThrow(() -> new InputDataException("Location with id" + id + "not exists"));
    }

    public List<LocationResponse> findAll(){
        return locationRepository.findAll().stream()
                .map(LocationResponse::new)
                .collect(Collectors.toList());
    }

    /*public DataResponse<LocationResponse> findAll(PaginationRequest paginationRequest){
        Page<Location> page = locationRepository.findAll(paginationRequest.toPageRequest());

        return new DataResponse<>(
                page.get().map(LocationResponse::new).collect(Collectors.toList()),
                page.getTotalPages(),
                page.getTotalElements()
        );
    }*/

    public Location findByRegion(String region){return locationRepository.findByRegion(region);}
    public Location findByCity(String city){return locationRepository.findByCity(city);}
}
