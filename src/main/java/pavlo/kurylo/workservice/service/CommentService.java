package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.CommentRequest;
import pavlo.kurylo.workservice.dto.response.CommentResponse;
import pavlo.kurylo.workservice.entity.Comment;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.CommentRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ServicesService servicesService;

    public CommentResponse save(CommentRequest commentRequest)throws InputDataException{
        return new CommentResponse(commentRepository.save(commentRequestToComment(commentRequest, null)));
    }

    public CommentResponse update(CommentRequest commentRequest, Long id)throws InputDataException{
        return new CommentResponse(commentRepository.save(commentRequestToComment(commentRequest,findOne(id))));
    }

    public List<CommentResponse> findAll(){
        return commentRepository.findAll().stream()
                .map(CommentResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id)throws InputDataException{commentRepository.delete(findOne(id));}

    private Comment commentRequestToComment(CommentRequest commentRequest, Comment comment)throws InputDataException{
        if (comment == null){
            comment = new Comment();
        }
        comment.setUsers(userService.findOne(commentRequest.getUserId()));
        comment.setText(commentRequest.getText());
        comment.setService(servicesService.findOne(commentRequest.getServiceId() ));
        return comment;
    }

    private Comment findOne(Long id)throws InputDataException{
        return commentRepository.findById(id).orElseThrow(() -> new InputDataException("Comment with id" + id + "not exists"));
    }
}
