package pavlo.kurylo.workservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pavlo.kurylo.workservice.dto.request.SubTypeRequest;
import pavlo.kurylo.workservice.dto.response.SubTypeResponse;
import pavlo.kurylo.workservice.entity.SubType;
import pavlo.kurylo.workservice.exseption.InputDataException;
import pavlo.kurylo.workservice.repository.SubTypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubTypeService {

    @Autowired
    private SubTypeRepository subTypeRepository;


    @Autowired
    private TypeService typeService;

    public SubTypeResponse save(SubTypeRequest subTypeRequest){
        return new SubTypeResponse(subTypeRepository.save(subTypeRequestToSubType(subTypeRequest,null)));
    }

    public SubTypeResponse update(SubTypeRequest subTypeRequest,Long id)throws InputDataException {
        return new SubTypeResponse(subTypeRepository.save(subTypeRequestToSubType(subTypeRequest,findOne(id))));
    }

    public void delete(Long id)throws InputDataException {
        subTypeRepository.delete(findOne(id));
    }

    public List<SubTypeResponse> findAll(){
        return subTypeRepository.findAll().stream()
                .map(SubTypeResponse::new)
                .collect(Collectors.toList());
    }

    private SubType subTypeRequestToSubType(SubTypeRequest subTypeRequest, SubType subType){
        if (subType == null){
            subType = new SubType();
        }
        subType.setTitle(subTypeRequest.getTitle());
        subType.setType(typeService.findByTitle(subTypeRequest.getTypeTitle()));
        return subType;
    }

    private SubType findOne(Long id)throws InputDataException {
        return subTypeRepository.findById(id).orElseThrow(()-> new InputDataException("SubType with id" + id + "not exists"));
    }
}
