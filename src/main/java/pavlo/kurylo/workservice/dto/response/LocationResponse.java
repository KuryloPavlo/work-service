package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Location;

@Getter @Setter
public class LocationResponse {
    private Long id;

    private String region;
    private String city;

    public LocationResponse (Location location) {
        id = location.getId();
        region = location.getRegion();
        city = location.getCity();
    }
}
