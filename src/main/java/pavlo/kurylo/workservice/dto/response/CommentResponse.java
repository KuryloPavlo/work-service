package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Comment;
import pavlo.kurylo.workservice.entity.Services;
import pavlo.kurylo.workservice.entity.User;

@Getter @Setter
public class CommentResponse {
    private  Long id;

    private Long userId;
    private String text;
    private Long serviceId;

    public CommentResponse(Comment comment) {
        id = comment.getId();
        userId = comment.getUsers().getId();
        text = comment.getText();
        serviceId = comment.getService().getId();
    }
}
