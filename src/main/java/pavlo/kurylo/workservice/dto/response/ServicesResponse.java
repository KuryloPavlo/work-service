package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Services;


@Getter @Setter
public class ServicesResponse {
    private Long id;

    private String title;
    private String description;
    private int salary;
    private boolean jobSearch;

    private Long userId;
    private Long locationId;
    private String typeTitle;

    public ServicesResponse(Services services){
        id = services.getId();
        title = services.getTitle();
        description = services.getDescription();
        salary = services.getSalary();
        jobSearch = services.isJobSearch();
        userId = services.getUser().getId();
        locationId = services.getLocation().getId();
        typeTitle = services.getType().getTitle();
    }
}
