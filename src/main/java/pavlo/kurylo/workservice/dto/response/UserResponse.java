package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.User;

@Getter @Setter
public class UserResponse {
    private Long id;

    private String name;
    private Integer phoneNumber;
    private String password;
    private String email;

    public UserResponse(User user){
        id = user.getId();
        name = user.getName();
        phoneNumber = user.getPhoneNumber();
        email = user.getEmail();
    }
}
