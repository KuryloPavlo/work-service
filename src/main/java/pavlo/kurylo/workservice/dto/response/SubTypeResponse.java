package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Services;
import pavlo.kurylo.workservice.entity.SubType;
import pavlo.kurylo.workservice.entity.Type;

@Getter @Setter
public class SubTypeResponse {
    private Long id;

    private String title;

    private String typeTitle;


    public SubTypeResponse(SubType subType){
        id = subType.getId();
        title = subType.getTitle();
        typeTitle = subType.getType().getTitle();
    }
}
