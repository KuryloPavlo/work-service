package pavlo.kurylo.workservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Type;

@Getter @Setter
public class TypeResponse {
    private Long id;

    private String title;

    public TypeResponse (Type type){
        id = type.getId();
        title = type.getTitle();
    }
}
