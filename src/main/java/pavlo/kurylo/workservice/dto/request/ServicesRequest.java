package pavlo.kurylo.workservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@Getter @Setter
public class ServicesRequest {

    @NotNull
    @NotBlank
    private String title;
    private String description;
    @Positive
    private int salary;
    private boolean jobSearch;

    private Long userId;
    private Long locationId;
    private String typeTitle;

}
