package pavlo.kurylo.workservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TypeRequest {
    @NotNull
    @NotBlank
    private String title;

}
