package pavlo.kurylo.workservice.dto.request;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LocationRequest {
    private String region;
    private String city;
}
