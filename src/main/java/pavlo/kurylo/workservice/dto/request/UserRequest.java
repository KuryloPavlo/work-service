package pavlo.kurylo.workservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter @Setter
public class UserRequest {
    @NotNull
    @NotBlank
    private String name;
    @Positive
    private Integer phoneNumber;
    private String password;
    @Email
    private String email;
}
