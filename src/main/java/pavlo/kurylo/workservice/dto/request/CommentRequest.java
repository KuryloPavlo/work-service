package pavlo.kurylo.workservice.dto.request;

import lombok.Getter;
import lombok.Setter;
import pavlo.kurylo.workservice.entity.Services;
import pavlo.kurylo.workservice.entity.User;

@Getter @Setter
public class CommentRequest {

    private Long userId;
    private String text;
    private Long serviceId;
}
