package pavlo.kurylo.workservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor

@Entity
public class Services {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String description;
    private int salary;
    private boolean jobSearch;

    @ManyToOne
    private User user;

    @ManyToOne
    private Location location;

    @OneToOne
    private Type type;
    @OneToMany
    private List<SubType> subType = new ArrayList<>();

    @OneToMany
    private List<Comment> comment = new ArrayList<>();
}
