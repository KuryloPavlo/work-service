package pavlo.kurylo.workservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(unique = true)
    private Integer phoneNumber;
    private String password;
    @Column(unique = true)
    private String email;

    @OneToMany
    private List<Services> service = new ArrayList<>();
}
